package user

import (
	"bo_ma/db"
	"bo_ma/models"
	"bo_ma/shared/apperrors"
	"errors"
	"fmt"
	"log"
	"time"
)

// Create -
func Create(data models.UserModel) (int, error) {
	res, err := db.DB.Exec("INSERT INTO users VALUES(NULL, ?,?,?,?,?,NULL, ?)",
		data.FirstName,
		data.LastName,
		data.Address,
		data.Email,
		data.Phone,
		data.PasswordHash,
	)

	if err != nil {
		log.Println(err)

		return -1, errors.New("Error occurred creating user data")
	}

	generatedID, err := res.LastInsertId()

	if err != nil {
		return -1, errors.New("Error occurred creating user")
	}

	return int(generatedID), nil
}

// UpdatePasswordHash -
func UpdatePasswordHash(id int, hash string) error {
	nowLastUpdated := time.Now().Unix()
	res, err := db.DB.Exec("UPDATE users SET `password_hash`=?, `last_updated_ts`=? WHERE id=?;",
		hash,
		nowLastUpdated,
		id,
	)

	if err != nil {
		log.Println(err)

		return errors.New("Error occurred updating user data")
	}

	if rowsAffected, _ := res.RowsAffected(); rowsAffected == 0 {
		return &apperrors.RecordNotFoundError{ID: id, Entity: "User"}
	}

	return nil
}

// UpdateByID -
func UpdateByID(id int, data models.UserModel) error {
	nowLastUpdated := time.Now().Unix()
	res, err := db.DB.Exec("UPDATE users SET `first_name`=?, `last_name`=?, `address`=?, `email`=?, `phone`=?, `last_updated_ts`=? WHERE id=?;",
		data.FirstName,
		data.LastName,
		data.Address,
		data.Email,
		data.Phone,
		nowLastUpdated,
		id,
	)

	if err != nil {
		log.Println(err)

		return errors.New("Error occurred updating user data")
	}

	if rowsAffected, _ := res.RowsAffected(); rowsAffected == 0 {
		return &apperrors.RecordNotFoundError{ID: id, Entity: "User"}
	}

	return nil
}

// GetByEmail - returns user model data by PK
func GetByEmail(email string) (*models.UserModel, error) {
	var user models.UserModel
	err := db.DB.QueryRow("SELECT * from users where email=?", email).Scan(
		&user.ID,
		&user.FirstName,
		&user.LastName,
		&user.Address,
		&user.Email,
		&user.Phone,
		&user.LastUpdatedTS,
		&user.PasswordHash,
	)

	if err != nil {
		fmt.Println(err)
		if err.Error() == "sql: no rows in result set" { // TODO: extract to db error tokens & re-use to avoid magic strings
			return nil, &apperrors.RecordNotFoundError{Entity: "User"}
		}

		return nil, errors.New("Error occurred fetching user data")
	}

	return &user, nil
}

// GetByID - returns user model data by PK
func GetByID(id int) (*models.UserModel, error) {
	var user models.UserModel
	err := db.DB.QueryRow("SELECT * from users where id=?", id).Scan(
		&user.ID,
		&user.FirstName,
		&user.LastName,
		&user.Address,
		&user.Email,
		&user.Phone,
		&user.LastUpdatedTS,
		&user.PasswordHash,
	)

	if err != nil {
		fmt.Println(err)
		if err.Error() == "sql: no rows in result set" { // TODO: extract to db error tokens & re-use to avoid magic strings
			return nil, &apperrors.RecordNotFoundError{ID: id, Entity: "User"}
		}

		return nil, errors.New("Error occurred fetching user data")
	}

	return &user, nil
}
