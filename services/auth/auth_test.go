package auth

import (
	"testing"
)

func TestCheckPasswordHash(t *testing.T) {

	t.Run("generated password check succeeds", func(t *testing.T) {
		originalPassword := "123abcd_"

		hash, err := GeneratePasswordHash(originalPassword)

		if err != nil {
			t.Error(err)
		}

		matched := CheckPasswordHash(originalPassword, hash)

		if !matched {
			t.Error("Password check failed, doesn't match")
		}
	})

	t.Run("Modified hash check fails", func(t2 *testing.T) {
		originalPassword := "123abcd_"
		hash, _ := GeneratePasswordHash(originalPassword)
		modifiedHash := hash[:len(hash)-1]
		matchedModified := CheckPasswordHash(originalPassword, modifiedHash)

		if matchedModified {
			t.Error("Password check failed, modified hash expected not to match")
		}
	})

}

func TestGeneratePasswordHash(t *testing.T) {
	t.Run("Fails if empty password provided", func(t *testing.T) {
		_, err := GeneratePasswordHash("")

		if err == nil {
			t.Fail()
		}
	})

	t.Run("Returns hash", func(t *testing.T) {
		password := "SOME_STRING"
		v, err := GeneratePasswordHash(password)

		if err != nil {
			t.Error("Expected not to fail", err)
		}

		if v == "" || v == password {
			t.Error("Expected to create hash", err)
		}
	})
}
