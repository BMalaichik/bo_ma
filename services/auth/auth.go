package auth

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
)

// GeneratePasswordHash -
func GeneratePasswordHash(password string) (string, error) {
	if password == "" {
		return "", errors.New("Empty password value provided")
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)

	return string(bytes), err
}

// CheckPasswordHash -
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	return err == nil
}
