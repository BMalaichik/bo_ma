package session

import (
	"bo_ma/config"
	"bo_ma/config/tokens"
	"bo_ma/models"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"strconv"
	"time"
)

// Info - represents session state info
type Info struct {
	CreatedAt int64 // unix timestamp
	Email     string
	ID        int
}

// Represents storage map.
var sessionStorage = make(map[string]*Info)

type manager struct {
}

// Manager - provides Session Management API
var Manager manager

// Get - obtain session token value
func (m *manager) Get(key string) (*Info, error) {
	value, ok := sessionStorage[key]

	if !ok || value == nil {
		return nil, errors.New("No session found")
	}

	return value, nil
}

// SetByKey - persist session token value by specified key. Might be useful to use with built keys by pattern
func (m *manager) SetByKey(key string, u models.UserModel) string {
	sessionStorage[key] = &Info{
		CreatedAt: time.Now().Unix(),
		Email:     u.Email,
		ID:        u.ID,
	}

	return key
}

// Set - persist session token value and returns session key
func (m *manager) Set(u models.UserModel) string {
	key := m.GenerateKey()
	sessionStorage[key] = &Info{
		CreatedAt: time.Now().Unix(),
		Email:     u.Email,
		ID:        u.ID,
	}

	return key
}

// Delete - removes session entry
func (m *manager) Delete(key string) {
	delete(sessionStorage, key)
}

func (m *manager) GenerateKey() string {
	b := make([]byte, 16)
	rand.Read(b)

	return base64.URLEncoding.EncodeToString(b)
}

// CalculateExpirationTime
func (m *manager) CalculateExpirationTime(t time.Time) time.Time {
	cookieExpiresInMins, _ := strconv.Atoi(config.Get(tokens.AUTHCookieExpiresMins))
	d := t.Add(time.Duration(cookieExpiresInMins) * time.Minute)
	return d
}

func (m *manager) Validate(key string) (*Info, error) {
	info, err := m.Get(key)

	if err != nil {
		return nil, err
	}

	createdAtTime := time.Unix(info.CreatedAt, 0)
	expectedExpirationTime := m.CalculateExpirationTime(createdAtTime)

	if expectedExpirationTime.Before(time.Now()) {
		return nil, errors.New("Expired")
	}

	return info, nil
}
