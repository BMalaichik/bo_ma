package apperrors

import (
	"fmt"
	"strconv"
)

// RecordNotFoundError -
type RecordNotFoundError struct {
	Entity string
	ID     int
}

func (e *RecordNotFoundError) Error() string {
	var idString string

	if e.ID != 0 {
		idString = " with ID #" + strconv.Itoa(e.ID)
	}

	return fmt.Sprintf("%v record%v not found", e.Entity, idString)
}
