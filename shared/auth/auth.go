package auth

import (
	"bo_ma/config"
	"bo_ma/config/tokens"
	"bo_ma/shared/session"
	"bo_ma/models"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/context"
)

/**
Auth-related tokens
*/
const (
	UserContextKey = "user"
)

// GetCurrentUserInfo - return request-context user data
func GetCurrentUserInfo(r *http.Request) *session.Info {
	data, ok := context.GetOk(r, UserContextKey)

	if !ok {
		return nil
	}

	return data.(*session.Info)
}

// ClearAuthCookie - modifies response, set app auth cookie
func ClearAuthCookie(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:     config.Get(tokens.AUTHCookieName),
		MaxAge:   0,
		Value:    "",
		HttpOnly: true,
		Path:     "/",
	})
}

// SetResponseAuthCookie - modifies response, set app auth cookie
func SetResponseAuthCookie(w http.ResponseWriter, u models.UserModel) {
	sessionKey := session.Manager.Set(u)
	cookieExpirationTime := session.Manager.CalculateExpirationTime(time.Now())

	http.SetCookie(w, &http.Cookie{
		Name:     config.Get(tokens.AUTHCookieName),
		Expires:  cookieExpirationTime,
		Value:    sessionKey,
		HttpOnly: true,
		Path:     "/",
	})
}

// GetAuthCookie parses request & return auth cookie value
func GetAuthCookie(r *http.Request) *http.Cookie {
	cookie, _ := r.Cookie(config.Get(tokens.AUTHCookieName))

	return cookie
}

// AuthenticateMiddleware - resolves user context data
func AuthenticateMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie := GetAuthCookie(r)

		if cookie == nil {
			next.ServeHTTP(w, r)
			return
		}

		sessionKey := cookie.Value
		info, err := session.Manager.Get(sessionKey)

		if err == nil {
			context.Set(r, UserContextKey, info)
		}

		next.ServeHTTP(w, r)
	})
}

// AuthorizeMiddleware - validates user session
func AuthorizeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Validate if session data resolved
		_, ok := context.GetOk(r, UserContextKey)

		if !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)

			return
		}

		// Validate is session expired

		cookie := GetAuthCookie(r)
		sessionKey := cookie.Value

		_, err := session.Manager.Validate(sessionKey)

		if err != nil {
			log.Println("Failed authorize attempt:", err)
			http.Error(w, "Forbidden", http.StatusForbidden)

			return
		}

		next.ServeHTTP(w, r)
	})
}
