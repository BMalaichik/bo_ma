package auth

import (
	"bo_ma/config"
	"bo_ma/config/tokens"
	"fmt"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var redirectURL = fmt.Sprintf("%v/api/auth/google/callback", config.Get(tokens.HOST))

// GoogleOauthConfig - OAuth2 google configuration
var GoogleOauthConfig = &oauth2.Config{
	RedirectURL:  redirectURL,
	ClientID:     config.Get(tokens.GoogleOAuthClientID),
	ClientSecret: config.Get(tokens.GoogleOAuthClientSecret),
	Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint:     google.Endpoint,
}

// GooleAPIUrl - authentication GAPI url
var GooleAPIUrl = config.Get(tokens.GooleOAuthAPIUrl)
