package mailer

import (
	"bo_ma/config"
	"bo_ma/config/tokens"
	"errors"
	"fmt"
	"net/smtp"
)

var client smtp.Auth
var sender = config.Get(tokens.MailingSender)

func init() {
	client = smtp.PlainAuth(
		"",
		config.Get(tokens.MailingUser),
		config.Get(tokens.MailingPassword),
		config.Get(tokens.MailingHost),
	)
}

// Send - send deserialized email body to recipient
func Send(recipient string, subject string, body []byte) error {
	headerString := fmt.Sprintf("From: App %v\r\n"+
		"To: %v\r\n"+
		"Subject: %v!\r\n"+
		"\r\n", sender, recipient, subject)
	header := []byte(headerString)
	fullBody := append(header, body...)
	err := smtp.SendMail(
		fmt.Sprintf("%v:%v", config.Get(tokens.MailingHost), config.Get(tokens.MailingPort)),
		client,
		sender,
		[]string{recipient},
		fullBody,
	)

	if err != nil {
		fmt.Println(err)
		return errors.New("Error occurred sending email")
	}

	return nil
}
