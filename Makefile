.PHONY: cleanup, build

GOBIN=go
BUILT_DIR=./bin

cleanup:
	@rm -rf ${BUILT_DIR}

install:
	@${GOBIN} get
	@echo "Install completed"

test:
	# setting APP_ROOT to properly resolve configuration package if consumer was launched from non-root
	@APP_ROOT=$${PWD} ${GOBIN} test -v -cover ./...

build: cleanup, install
	@${GOBIN} build -o ${BUILT_DIR}/main ./main.go

run: build
	@${BUILT_DIR}/main
