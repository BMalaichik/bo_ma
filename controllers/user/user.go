package user

import (
	"bo_ma/models"
	userService "bo_ma/services/user"
	"bo_ma/shared/auth"
	"bo_ma/templates"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Setup -
func Setup(r *mux.Router) {
	userRouter := r.Name("controller-user").PathPrefix("/user").Subrouter()
	userRouter.Use(auth.AuthorizeMiddleware)

	userRouter.HandleFunc("/{id:[0-9]+}/view", profileView)
	userRouter.HandleFunc("/{id:[0-9]+}/edit", profileEdit)
}

func profileView(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("user", "view")

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	user := resolveUserPersonalData(w, req)

	if user == nil {
		return
	}

	err = tmpl.Execute(w, user)

	if err != nil {
		fmt.Println(err)
	}
}

// ProfileEdit - user profile view page
func profileEdit(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("user", "edit")

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	user := resolveUserPersonalData(w, req)

	err = tmpl.Execute(w, user)

	if err != nil {
		fmt.Println(err)
	}
}

func resolveUserPersonalData(w http.ResponseWriter, req *http.Request) *models.UserModel {
	userInfo := auth.GetCurrentUserInfo(req)
	user, _ := userService.GetByEmail(userInfo.Email)
	requestedID := mux.Vars(req)["id"]
	requestedIDInt, _ := strconv.Atoi(requestedID)

	if requestedIDInt != user.ID {
		http.Error(w, "Forbidden", http.StatusForbidden)

		return nil
	}

	return user
}
