package home

import (
	"bo_ma/templates"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Setup -
func Setup(r *mux.Router) {
	r.HandleFunc("/login", loginView).Methods(http.MethodGet)
	r.HandleFunc("/signup", signupView).Methods(http.MethodGet)
	r.HandleFunc("/reset-password", resetPasswordView).Methods(http.MethodGet)
}

func resetPasswordView(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("auth", "reset-password")

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	d := map[string]string{
		"Title": "Reset Password Request",
	}

	err = tmpl.Execute(w, d)

	if err != nil {
		fmt.Println(err)
	}
}

func loginView(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("auth", "login")

	// TODO: extract to helper http function ? validate template resolution ?
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}
	d := map[string]string{
		"Title": "Login",
	}
	err = tmpl.Execute(w, d)

	if err != nil {
		fmt.Println(err)
	}
}

func signupView(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("auth", "signup")

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}
	d := map[string]string{}
	err = tmpl.Execute(w, d)

	if err != nil {
		fmt.Println(err)
	}
}
