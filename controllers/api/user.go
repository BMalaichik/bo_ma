package api

import (
	"bo_ma/models"
	userService "bo_ma/services/user"
	"bo_ma/shared/auth"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func setupUserRouter(r *mux.Router) {
	ur := r.Name("user").PathPrefix("/user").Subrouter()

	ur.Use(auth.AuthorizeMiddleware)
	ur.HandleFunc("", createUserHandler).Methods(http.MethodPost)
	ur.HandleFunc("/{id:[0-9]+}", getUserHandler).Methods(http.MethodGet)
	ur.HandleFunc("/{id:[0-9]+}", updateUserHandler).Methods(http.MethodPost)
}

func createUserHandler(w http.ResponseWriter, req *http.Request) {
	data, err := parseRequestUserData(w, req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
	}

	id, err := userService.Create(*data)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	data.ID = id

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)

	ReturnUserData(w, data)
}

func updateUserHandler(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	userIDString := params["id"]
	userID, _ := strconv.Atoi(userIDString)

	data, err := parseRequestUserData(w, req)
	data.ID = userID

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))

		return
	}

	err = userService.UpdateByID(userID, *data)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)

	ReturnUserData(w, data)
}

func getUserHandler(w http.ResponseWriter, req *http.Request) {
	userIDString := mux.Vars(req)["id"]
	userID, _ := strconv.Atoi(userIDString)
	user, err := userService.GetByID(userID)

	if user == nil {
		http.Error(w, "User not found", http.StatusNotFound)

		return
	}

	if err != nil {
		log.Println(err)
		http.Error(w, "Something went wrong", http.StatusUnprocessableEntity)

		return
	}

	w.Header().Set("content-type", "application/json")

	ReturnUserData(w, user)
}

// ReturnUserData - serializes user data and returns ad json
func ReturnUserData(w http.ResponseWriter, data *models.UserModel) {
	userBytes, err := json.MarshalIndent(data, "", "\t")

	if err != nil {
		log.Print("Error occurred marshalling user data")
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	w.Write(userBytes)
}

func parseRequestUserData(w http.ResponseWriter, req *http.Request) (*models.UserModel, error) {
	err := req.ParseForm()

	if err != nil {
		msg := "Error occurred parsing user data"
		log.Println(msg, err)

		return nil, errors.New(msg)
	}

	data := models.UserModel{
		FirstName: req.PostFormValue("firstName"),
		LastName:  req.PostFormValue("lastName"),
		Address:   req.PostFormValue("address"),
		Phone:     req.PostFormValue("phone"),
		Email:     req.PostFormValue("email"),
	}

	return &data, nil
}
