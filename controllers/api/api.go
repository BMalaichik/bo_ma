package api

import (
	"github.com/gorilla/mux"
)

// Setup -
func Setup(baseRouter *mux.Router) {
	setupUserRouter(baseRouter)
	setupAuthRouter(baseRouter)
}
