package api

import (
	"bo_ma/config"
	"bo_ma/config/tokens"
	"bo_ma/models"
	authService "bo_ma/services/auth"
	userService "bo_ma/services/user"
	"bo_ma/shared/apperrors"
	"bo_ma/shared/auth"
	"bo_ma/shared/mailer"
	"bo_ma/shared/session"
	"bo_ma/templates"
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

const (
	resetPasswordQueryToken = "token"
)

func setupAuthRouter(r *mux.Router) {
	ur := r.Name("auth").PathPrefix("/auth").Subrouter()

	ur.HandleFunc("/signup", registerHandler).Methods(http.MethodPost)
	ur.HandleFunc("/login", loginHandler).Methods(http.MethodPost)
	ur.HandleFunc("/reset-password", resetPasswordHandler).Methods(http.MethodPost)
	ur.HandleFunc("/reset-password/confirm", resetPassordConfirmHandler).Methods(http.MethodGet)
	ur.HandleFunc("/logout", logoutHandler).Methods(http.MethodPost, http.MethodGet)
	ur.HandleFunc("/google/login", googleLogin).Methods(http.MethodGet, http.MethodPost)
	ur.HandleFunc("/google/callback", oauthGoogleCallback)
}

func resetPassordConfirmHandler(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get(resetPasswordQueryToken)

	if token == "" {
		returnBadRequest(w, "Reset token is invalid or missing")

		return
	}

	userInfo, err := session.Manager.Get(token)

	if err != nil {
		returnBadRequest(w, "Reset token is invalid or missing")

		return
	}

	user, err := userService.GetByID(userInfo.ID)

	if err != nil {
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)

		return
	}

	// again, reusing for time-saving purposes...
	newPassword := session.Manager.GenerateKey()
	newPasswordHash, _ := authService.GeneratePasswordHash(newPassword)

	user.PasswordHash = newPasswordHash
	err = userService.UpdatePasswordHash(user.ID, newPasswordHash)

	if err != nil {
		fmt.Println("Error occurred updating user password hash:", err)
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)

		return
	}
	tmpl, err := templates.Resolve("email", "reset-password-confirmed")

	if err != nil {
		log.Println("Error occurred resolving reset password email:", err)
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)
		return
	}

	var buf bytes.Buffer
	loginURL := fmt.Sprintf("%v/login", config.Get(tokens.HOST))
	err = tmpl.Execute(&buf, map[string]interface{}{
		"Username": user.FirstName + " " + user.LastName,
		"Login":    user.Email,
		"Password": newPassword,
		"URL":      loginURL,
	})

	if err != nil {
		log.Println("Error occurred rendeting reset password coonfirmed email")
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)

		return
	}

	mailer.Send(user.Email, "Reset Password Request", buf.Bytes())

	session.Manager.Delete(token)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Email with new password has been sent to:" + user.Email))
}

func resetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()

	if err != nil {
		returnBadRequest(w, "Invalid Request")

		return
	}

	email := r.FormValue("email")

	if email == "" {
		returnBadRequest(w, "Invalid Request")

		return
	}

	user, err := userService.GetByEmail(email)

	if err != nil {
		log.Println(err)
		// BTW: part of requirements was to send response that email is not found in this case.
		// I don't encourage such approach because it gives a solid ground for potential bruteforce
		// to get user list.
		http.Error(w, "User with specified email not found", http.StatusNotFound)

		return
	}

	// lazy, re-using session manager key generator, only applicable for demo purposes
	// for resting purposes token persisting in server would be done via session storage manager
	// better approach is to persist this token in database in separate table(token ,userID, issuedAt columns)
	// and validate it.
	token := session.Manager.GenerateKey()

	tmpl, err := templates.Resolve("email", "reset-password")

	if err != nil {
		log.Println("Error occurred resolving reset password email:", err)
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)
		return
	}

	var buf bytes.Buffer
	resetLink := fmt.Sprintf("%v/api/auth/reset-password/confirm?%v=%v", config.Get(tokens.HOST), resetPasswordQueryToken, token)

	tmpl.Execute(&buf, map[string]string{
		"Link":     resetLink,
		"Username": user.FirstName + " " + user.LastName,
	})

	session.Manager.SetByKey(token, *user)

	err = mailer.Send(user.Email, "Reset Password Request", buf.Bytes())

	if err != nil {
		log.Println("Error occurred sending reset password email:", err)
		http.Error(w, "Something went wrong. Please, try again later", http.StatusUnprocessableEntity)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Reset email has been successfully sent."))
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	user := auth.GetCurrentUserInfo(r)

	if user == nil {
		return
	}

	cookie := auth.GetAuthCookie(r)

	if cookie == nil {
		return
	}

	session.Manager.Delete(cookie.Value)
	auth.ClearAuthCookie(w)
	http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
}

/**
Same API with redirect callback is used for both login & signup via google.
If users already exists - login performed.
If user is new - user record is created(user is signed-up) & signed-in
*/
func googleLogin(w http.ResponseWriter, r *http.Request) {
	oauthState := generateStateOauthCookie(w)
	url := auth.GoogleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	d, _ := time.ParseDuration(config.Get(tokens.GoogleOAuthCookieExpirationHours) + "h")
	var expiration = time.Now().Add(d)

	b := make([]byte, 16)
	rand.Read(b)
	cookieValue := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{
		Name:    config.Get(tokens.GoogleOAuthCookieName),
		Value:   cookieValue,
		Expires: expiration,
	}
	http.SetCookie(w, &cookie)

	return cookieValue
}

func oauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	oauthState, err := r.Cookie(config.Get(tokens.GoogleOAuthCookieName))

	if err != nil {
		log.Println(err)
		http.Error(w, "OAuth error", http.StatusUnprocessableEntity)

		return
	}

	if r.FormValue("state") != oauthState.Value {
		http.Error(w, "State Mismatch", http.StatusUnauthorized)

		return
	}

	data, err := getUserDataFromGoogle(r.FormValue("code"))

	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)

		return
	}

	gUserData := make(map[string]string)
	json.Unmarshal(data, &gUserData)
	existingUser, err := userService.GetByEmail(gUserData["email"])

	if _, ok := err.(*apperrors.RecordNotFoundError); ok {
		// Registering new user in our system
		userDataToCreate := models.UserModel{
			Email: gUserData["email"],
		}
		createdID, err := userService.Create(userDataToCreate)
		existingUser, _ = userService.GetByID(createdID)
		if err != nil {
			log.Println(err)
			http.Error(w, "OAuth error", http.StatusUnprocessableEntity)

			return
		}
	}

	auth.SetResponseAuthCookie(w, *existingUser)
	http.Redirect(w, r, fmt.Sprintf("/user/%v/view", existingUser.ID), http.StatusTemporaryRedirect)
}

func getUserDataFromGoogle(code string) ([]byte, error) {
	token, err := auth.GoogleOauthConfig.Exchange(context.Background(), code)

	if err != nil {
		fmt.Println(err)

		return nil, errors.New("Error occurred exchaning user code: ")
	}

	response, err := http.Get(auth.GooleAPIUrl + token.AccessToken)

	if err != nil {
		fmt.Println(err)

		return nil, errors.New("Error occurred obtaining user code: ")
	}

	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	return contents, nil
}

func loginHandler(w http.ResponseWriter, req *http.Request) {
	data := models.LoginModel{
		Email:    req.PostFormValue("email"),
		Password: req.PostFormValue("password"),
	}

	if data.Email == "" || data.Password == "" {
		returnBadRequest(w, errors.New("Invalid request data"))

		return
	}

	user, err := userService.GetByEmail(data.Email)

	if err != nil {
		returnBadRequest(w, err)

		return
	}

	passwordMatches := authService.CheckPasswordHash(data.Password, user.PasswordHash)

	if !passwordMatches {
		log.Println("Failed login attempt", data.Email) // TODO: add more info in failed attempt. time, ip and etc.
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Unauthorized"))

		return
	}

	auth.SetResponseAuthCookie(w, *user)
	http.Redirect(w, req, fmt.Sprintf("/user/%v/view", user.ID), http.StatusFound)
}

func registerHandler(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()

	if err != nil {
		returnBadRequest(w, err)

		return
	}

	data := models.SignupModel{
		Password: req.FormValue("password"),
		Email:    req.FormValue("email"),
	}

	if data.Password == "" || data.Email == "" {
		returnBadRequest(w, "Invalid request")

		return
	}

	hash, err := authService.GeneratePasswordHash(data.Password)

	if err != nil {
		returnBadRequest(w, err) // TODO: replace with other code

		return
	}

	userData := models.UserModel{
		PasswordHash: hash,
		Email:        data.Email,
	}

	userID, err := userService.Create(userData)

	if err != nil {
		returnBadRequest(w, err)

		return
	}

	createdUser, _ := userService.GetByID(userID)

	auth.SetResponseAuthCookie(w, *createdUser)
	http.Redirect(w, req, fmt.Sprintf("/user/%v/edit", createdUser.ID), http.StatusTemporaryRedirect)
}

func returnBadRequest(w http.ResponseWriter, err interface{}) {
	log.Println(err)

	http.Error(w, "Bad Request", http.StatusBadRequest)
}
