package home

import (
	"bo_ma/shared/auth"
	"bo_ma/templates"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Setup -
func Setup(r *mux.Router) {
	r.HandleFunc("/", RedirectIndexView).Methods(http.MethodGet)
	r.HandleFunc("/home", IndexView).Methods(http.MethodGet)
}

// RedirectIndexView -
func RedirectIndexView(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, "/home", http.StatusPermanentRedirect)
}

// IndexView -
func IndexView(w http.ResponseWriter, req *http.Request) {
	tmpl, err := templates.Resolve("base", "index")

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte("Something went wrong"))

		return
	}

	viewData := map[string]interface{}{
		"Title": "Home",
		"Head":  "Welcome to App",
	}

	if user := auth.GetCurrentUserInfo(req); user != nil {
		viewData["ID"] = user.ID
	}

	err = tmpl.Execute(w, viewData)

	if err != nil {
		fmt.Println(err)
	}
}
