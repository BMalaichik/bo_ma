package tokens

/**
Set of config tokens to be used by config manager
*/
const (
	HOST                             = "host"
	PORT                             = "port"
	ENV                              = "env"
	DbHOST                           = "db_host"
	DbUSER                           = "db_user"
	DbPORT                           = "db_port"
	DbPASSWORD                       = "db_password"
	DbNAME                           = "db_name"
	AUTHCookieName                   = "auth_cookie_name"
	AUTHCookieExpiresMins            = "auth_cookie_expires_mins"
	GoogleOAuthClientID              = "g_oauth_client_id"
	GoogleOAuthClientSecret          = "g_oauth_client_secret"
	GoogleOAuthCookieName            = "g_oauth_cookie_name"
	GoogleOAuthCookieExpirationHours = "g_oauth_expires_h"
	GooleOAuthAPIUrl                 = "g_oauth_api_url"
	MailingHost                      = "mail_host"
	MailingSender                    = "mail_sender"
	MailingPort                      = "mail_port"
	MailingUser                      = "mail_user"
	MailingPassword                  = "mail_pwd"
)
