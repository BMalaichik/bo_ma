package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bo_ma/config/tokens"
)

var store = make(map[string]string)

// Get returns config value by key
func Get(key string) string {
	v, _ := store[key]

	return v
}

// Set sets config value by key
func Set(key string, value string) {
	store[key] = value
}

var env string
var envConfigPath string
var defaultEnv = "development"

func init() {
	appRootPath := os.Getenv("APP_ROOT")

	if appRootPath != "" {
		os.Chdir(appRootPath) // adding support for correct packages resolution running e2e specs
	}

	env := os.Getenv("ENV")

	if env == "" {
		env = defaultEnv
	}

	envConfigPath = fmt.Sprintf("./config/%v/config.json", env)

	envConfigFile, err := os.Open(envConfigPath)

	if err != nil {
		log.Fatal("Could not resolve ENV config, aborting...", err)
	}

	defer envConfigFile.Close()

	b, _ := ioutil.ReadAll(envConfigFile)

	err = json.Unmarshal(b, &store)

	if err != nil {
		log.Fatal("Could not parse ENV config, aborting...", err)
	}

	Set(tokens.ENV, env)
	Set(tokens.GoogleOAuthClientID, os.Getenv("GOOGLE_OAUTH_CLIENT_ID"))
	Set(tokens.GoogleOAuthClientSecret, os.Getenv("GOOGLE_OAUTH_CLIENT_SECRET"))
	Set(tokens.MailingUser, os.Getenv("MAILING_USER"))
	Set(tokens.MailingPassword, os.Getenv("MAILING_PASSWORD"))

	if err != nil {
		log.Fatal(err)
	}
}
