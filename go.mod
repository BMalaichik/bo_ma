module bo_ma

go 1.12

require (
	cloud.google.com/go v0.43.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.3
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
