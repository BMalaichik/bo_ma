package models

// SignupModel - represents signup data
type SignupModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
