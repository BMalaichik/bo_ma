package models

// LoginModel - represents signup data
type LoginModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
