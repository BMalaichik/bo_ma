package models

import "database/sql"

// UserModel - Represents user db data
type UserModel struct {
	ID            int           `json:"id"`
	FirstName     string        `json:"firstName"`
	LastName      string        `json:"lastName"`
	Address       string        `json:"address"`
	Email         string        `json:"email"`
	Phone         string        `json:"phone"`
	PasswordHash  string        `json:"-"`
	LastUpdatedTS sql.NullInt64 `json:"-"`
}
