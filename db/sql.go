package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"bo_ma/config"
	"bo_ma/config/tokens"

	_ "github.com/go-sql-driver/mysql" // Required 3-d party driver dependency
)

// DB - exposes access to data source
var DB *sql.DB

func init() {
	connectionStr := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		config.Get(tokens.DbUSER),
		config.Get(tokens.DbPASSWORD),
		config.Get(tokens.DbHOST),
		config.Get(tokens.DbPORT),
		config.Get(tokens.DbNAME),
	)
	db, err := sql.Open("mysql", connectionStr)

	if err != nil {
		log.Fatal("Error occurred estabslishing db connection: ", err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	err = db.PingContext(ctx)

	if err != nil {
		log.Fatal(err)
	}

	DB = db
}
