CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    address VARCHAR(255),
    email VARCHAR(255) NOT NULL UNIQUE,
    phone VARCHAR(255),
    last_updated_ts INT(11),
    password_hash VARCHAR(255),
    PRIMARY KEY (id)
)  ENGINE=INNODB;
