package e2e

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

// GetHTTPServer - returns fake http server
func GetHTTPServer(h http.Handler) *httptest.Server {
	srv := httptest.NewServer(h)

	return srv
}

// HitHTTPHandler - setup HTTP server, hits handlers & return response
func HitHTTPHandler(t *testing.T, h func(w http.ResponseWriter, req *http.Request)) *http.Response {
	server := GetHTTPServer(http.HandlerFunc(h))

	defer server.Close()

	res, err := http.Get(server.URL)

	if err != nil {
		t.Error(err)
	}

	return res
}
