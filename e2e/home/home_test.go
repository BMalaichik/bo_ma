package home_test

import (
	"bo_ma/controllers/home"
	"bo_ma/e2e"
	"io/ioutil"
	"log"
	"strings"
	"testing"
)

func TestHomeController(t *testing.T) {
	t.Run("/home", func(t *testing.T) {
		response := e2e.HitHTTPHandler(t, home.IndexView)
		homeBytes, err := ioutil.ReadAll(response.Body)
		response.Body.Close()

		if err != nil {
			log.Fatal(err)
		}

		homeHtml := string(homeBytes)

		if !strings.Contains(homeHtml, "Hello") {
			t.Error("Home/Index template not resolved")
		}

	})
}
