package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bo_ma/config"
	"bo_ma/config/tokens"
	"bo_ma/controllers/api"
	authController "bo_ma/controllers/auth"
	homeController "bo_ma/controllers/home"
	userController "bo_ma/controllers/user"
	"bo_ma/db"
	"bo_ma/shared/auth"

	"github.com/gorilla/mux"
)

func main() {
	db.DB.Ping()
	var wait time.Duration

	baseRouter := mux.NewRouter()
	baseRouter.Use(auth.AuthenticateMiddleware)

	apiRouter := baseRouter.Name("api").PathPrefix("/api").Subrouter()
	controllersRouter := baseRouter.Name("controllers").Subrouter()

	api.Setup(apiRouter)
	homeController.Setup(controllersRouter)
	userController.Setup(controllersRouter)
	authController.Setup(controllersRouter)

	srv := &http.Server{
		Handler:      baseRouter,
		Addr:         ":" + config.Get(tokens.PORT),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Println("Server is listening on " + srv.Addr)

	go func() {
		log.Fatal(srv.ListenAndServe())
	}()

	// Graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Interrupted, shutting down...")
	os.Exit(0)
}
