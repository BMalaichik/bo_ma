package templates

import (
	"errors"
	"fmt"
	"html/template"
	"log"
)

var headerPath = buildPath("base", "header")
var footerPath = buildPath("base", "footer")

// EmailResolveData - template info to be resolved
type EmailResolveData struct {
	Group string
	Name  string
}

// Resolve returns html template wrapper
func Resolve(group string, name string) (*template.Template, error) {
	tmpl, err := template.ParseFiles(buildPath(group, name), footerPath, headerPath)

	if err != nil {
		log.Println(err)
		msg := fmt.Sprintf("HTML template %v/%v not resolved", group, name)
		return nil, errors.New(msg)
	}

	return tmpl, nil
}

func buildPath(group string, name string) string {
	return fmt.Sprintf("./templates/%v/%v.html", group, name)
}
